import Paths from "../../data/paths.js";

let parent = null;
let currentParent = new Paths().user;
let pathHistory = [currentParent];
let curentHistoryIndex = 0;

function generatePath() {
  if (parent) {
    parent.remove();
  }

  document.getElementById("home").disabled =
    currentParent.path === `/home/${window.location.hostname}/`;
  document.getElementById("parent").disabled = currentParent.parent === null;
  document.getElementById("back").disabled = curentHistoryIndex <= 0;
  document.getElementById("next").disabled =
    curentHistoryIndex + 1 >= pathHistory.length;

  if (currentParent.children.length === 0) {
    document.getElementById("placeholder").classList.remove("hide");
    return;
  }

  document.getElementById("placeholder").classList.add("hide");
  parent = document.createElement("dl");
  document.getElementById("app-name").textContent = currentParent.path;
  parent.id = "folders";
  parent.classList.add("folder-container");

  currentParent.children.forEach(makeChild);

  document.getElementById("window").appendChild(parent);
}

function makeChild(child) {
  const box = document.createElement("div");
  box.classList.add("folder-container");
  const childBody = document.createElement("dt");
  const childName = document.createElement("dd");

  childName.onclick = childBody.onclick = () => openChild(child);

  childBody.classList.add("folder");
  childName.textContent = child.name;
  box.appendChild(childBody);
  box.appendChild(childName);
  parent.appendChild(box);
}

function openChild(child) {
  pathHistory.splice(curentHistoryIndex + 1);
  currentParent = child;
  addToHistory();
  document.getElementById("app-name").textContent = currentParent.path;
  generatePath();
}

function addToHistory() {
  pathHistory.splice(curentHistoryIndex + 1);
  pathHistory.push(currentParent);
  curentHistoryIndex += 1;
}

document.getElementById("home").onclick = () => {
  if (currentParent.path !== `/home/${window.location.hostname}/`) {
    currentParent = new Paths().user;
    pathHistory.push(currentParent);
    curentHistoryIndex += 1;
    generatePath();
  }
};

document.getElementById("back").onclick = () => {
  if (curentHistoryIndex - 1 >= 0) {
    currentParent = pathHistory[curentHistoryIndex - 1];
    curentHistoryIndex -= 1;
    generatePath();
  }
};

document.getElementById("next").onclick = () => {
  if (curentHistoryIndex + 1 < pathHistory.length) {
    currentParent = pathHistory[curentHistoryIndex + 1];
    curentHistoryIndex += 1;
    generatePath();
  }
};

document.getElementById("parent").onclick = () => {
  if (currentParent.parent) {
    currentParent = currentParent.parent;
    addToHistory();
    generatePath();
  }
};

generatePath();
