---

name: "FEATURE"
about: "Request for a new feature"
title: "[FEATURE REQUEST] "
ref: "main"
labels:

- enhancement

---

## Describe the feature.


## Why is it needed?


## Implementation suggestions (if any)


## Screenshots/ references to help with the implementation.


## Applicable browsers

- [ ] Firefox (or its forks)
- [ ] Chromium (or its forks)
- [ ] Safari


## Applicable Platform
- [ ] Desktop/Laptop
- [ ] Mobile
- [ ] Tablets


## Additional information


## Checks

- [ ] I have gone through all existing open & closed feature requests and this feature is unique.
- [ ] This is NOT a BUG.
