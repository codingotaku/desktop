---

name: "BUG"
about: "Report a bug"
title: "[BUG] "
ref: "main"
labels:

- bug
- "help needed"

---

## Describe the problem


## What have you tried?


## How to reproduce?


## Applicable browsers

- [ ] Firefox (or its forks)
- [ ] Chromium (or its forks)
- [ ] Safari


## Applicable Platform
- [ ] Desktop/Laptop
- [ ] Mobile
- [ ] Tablets


## Additional information

## Checks

- [ ] I have gone through all existing open & closed issues and this issue is unique.
- [ ] The issue is relevent to this project and not a platform/ browser issue.
- [ ] This is NOT a feature request.
